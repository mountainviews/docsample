# Mountainviews Documentation

This is a sample repository to demonstrate how gitlab can be used to
store documentation.

[Markdown](http://daringfireball.net/projects/markdown/) can be used to
style text files, and they will be rendered by gitlab when viewed
online.

[entry1](sysadminlog/entry1.md) - files within the repository can be linked to 
from text documents